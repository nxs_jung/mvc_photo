<?php

class Shop extends Controller{

    protected $sTemplateTpl = 'cart.tpl';

    private $cart;
    private $iban;
    private $debit;
    private $validator;

    public function init(){
        $session = Registry::make('Session');
        if($session->is_logged_in()){
            $session->cart = $this->cart = new Cart($session->user_id);
        }else{
            $this->cart = new Cart();
        }
        return parent::init();
    }

    public function add_to_cart(){
        if(isset($_GET['id']) && isset($_GET['type'])){
            $id = $_GET['id'];
            $type = $_GET['type'];

            if($type == "photo"){
                $product = Photograph::find_by_id($id);
            }else{
                $product = Video::find_by_id($id);
            }

            $this->cart->add_product($product);
            $this->show();
        }
    }

    public function delete_from_basket()
    {
        if(isset($_GET['id'])){
            $id = $_GET['id'];

            $cart = Cart::find_by_id($id);
            $cart->delete();
        }
        $this->show();
    }

    public function show(){
        $output = $this->cart->display_basket();

        $this->addTplParam( 'cart', $output );
        $this->sTemplateTpl = 'cart.tpl';
    }

    public function checkout()
    {



        if(isset($_POST['submit'])){
        // Validation von Kreditkarte oder Lastschrift
        // eventuell auch Adresse eintragen

        $this->validator = new Payment_Validator();
        $message = $this->validator->validate_pay($_POST);
        var_dump($message);

        }else{
        //Alle Produkte für einen User holen
            $this->show();
            $this->sTemplateTpl = 'cart_checkout.tpl';

            $session = Registry::make('session');
            $user = User::find_by_id($session->user_id);
            if($user){
                $user->init_adresse();
                $this->addTplParam( 'user', $user );
                $this->addTplParam( 'debit', $this->debit );
                $this->addTplParam( 'iban', $this->iban );
            }else{
                $this->addTplParam( 'message', "Bitte tragen Sie unten ihre Kontaktdaten ein." );
            }
        }
    }

    public function set_payment(){
        if($_POST['payment']  == "debit"){
            $this->debit = true;
            $this->iban = false;
            $this->addTplParam( 'debit', $this->debit );
            $this->addTplParam( 'iban', $this->iban );
        }else{
            $this->debit = false;
            $this->iban = true;
            $this->addTplParam( 'debit', $this->debit );
            $this->addTplParam( 'iban', $this->iban );
        }

        $this->checkout();
    }

    public function render()
    {

        return parent::render();
    }

}