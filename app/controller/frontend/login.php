<?php

class Login extends Controller{

    protected $sTemplateTpl = 'login.tpl';

    private $user;

    public function login_user()
    {
        $session = Registry::make('Session');
        if(isset($_POST['submit'])){
            $username = $_POST['username'];
            $password = $_POST['password'];
            $user = User::authenticate($username, $password);
            if($user instanceOf User){
                $session->login($user);
                $this->oLogger->log_action("Login", "UserID ".$user->id." | The User '".$user->username."' logged in !");
            }
        }
    }

    public function logout(){
        $session = Registry::make('Session');

        $this->oLogger->log_action("Logout", "UserID ".$session->user_id." | The User logged out !");
        $session->logout();
    }


    public function render()
    {
        $session = Registry::make('Session');
        if($session->is_logged_in()){

            $user = User::find_by_id($session->user_id);
        }
        if(isset($user) && $user instanceOf User){
            redirect_to('index.php?cl=HomeBack');
        }else{
            $this->oLoad->area = "frontend";
        }
        return parent::render();
    }

}