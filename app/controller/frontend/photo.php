<?php

class Photo extends Controller{

    protected $sTemplateTpl = 'photo.tpl';

    private $photo;
    private $comments;

    public function show(){
        $this->photo = Photograph::find_by_id($_GET['id']);
        if(!$this->photo){
            redirect_to('index.php');
        }

        $session = Registry::make('Session');

        if ($session->is_logged_in()) {
            $user = User::find_by_id($session->user_id);
            $this->addTplParam( 'user', $user );
        }
        $this->comments = $this->photo->comments();
    }

    public function comment(){
        $message ="";
        $author= "";
        $body= "";
        if(isset($_POST['submit'])){
            $this->photo = Photograph::find_by_id($_GET['id']);
            $author = trim($_POST['author']);
            $body = trim($_POST['body']);

            $new_comment = Comment::make($this->photo->id, $author, $body);

            if($new_comment && $new_comment->save()){
                // comment saved
                // No message needed

                // Send email
                //$new_comment->try_to_send_notification();
                $this->oLogger->log_action("New Comment Photo", "Author ". $author ." | Added a new comment to ".$this->photo->filename."!");

                // Important! You could just let the page render from here.
                // But then if the page is reloaded, the form will try
                // to resubmit the comment. so redirect instead:
                redirect_to("index.php?cl=photo&fnc=show&id=".$this->photo->id);
            }else{
                // failed
                $message = "There was an error that prevented the comment from being saved.";
            }
        }
        $this->addTplParam( 'author', $author );
        $this->addTplParam( 'body', $body );
        $this->addTplParam( 'message', $message );
    }

    public function render()
    {
        $this->addTplParam( 'photo', $this->photo );
        $this->addTplParam( 'comments', $this->comments );
        return parent::render();
    }
}