<?php

class Home extends Controller
{
    protected $sTemplateTpl = 'home.tpl';


    public function render()
    {
        $photo_set = Photograph::find_all();
        $video_set = Video::find_all();
        $session = Registry::make('Session');

        $this->addTplParam( 'photos', $photo_set );
        $this->addTplParam( 'videos', $video_set );
        $this->addTplParam( 'logged_in', $session->is_logged_in() );
        return parent::render();
    }
}