<?php

class HomeBack extends BackendController
{
    protected $sTemplateTpl = 'index.tpl';

    private $session;
    private $user;

    public function render()
    {
        $this->session = Registry::make('Session');

        if($this->session->is_logged_in()){
            $this->user = User::find_by_id($this->session->user_id);
        }

        $this->addTplParam( 'session', $this->session );
        $this->addTplParam( 'user', $this->user );

        return parent::render();
    }
}