<?php

class VideoList extends BackendController{

    protected $sTemplateTpl = 'video_list.tpl';

    private $videos;
    private $aUser;
    private $message;
    private $max_file_size = 10485760;

    public function init()
    {
        $session = Registry::make('Session');
        $this->aUser = User::find_by_id($session->user_id);
        return parent::init();
    }

    public function showAll(){
        $this->sTemplateTpl = 'video_list.tpl';
        $this->videos = Video::find_all();
        if(!$this->videos){
            $this->message = "No videos avaible!";
            redirect_to('index.php?cl=homeback');
        }

        $session = Registry::make('Session');
        $this->aUser = User::find_by_id($session->user_id);
    }

    public function delete()
    {
        if(!isset($_GET['id'])){
            $this->message = "No video ID was provided.";
            redirect_to("index.php?cl=videolist&fnc=showAll");
        }

        $video = Video::find_by_id($_GET['id']);
        if($video && $video->destroy()){
            $this->message = "The video was deleted.";
            $this->oLogger->log_action("Delete", "UserID ". $this->aUser->id ."| Deleted a file (".$video->filename.") !");
            redirect_to('index.php?cl=videolist&fnc=showAll');
        }else{
            $this->message = "The video could not be deleted.";
            redirect_to('index.php?cl=videolist&fnc=showAll');
        }
    }

    public function create()
    {
        if(isset($_POST['submit'])){
            $video = new Video();
            $video->caption = $_POST['caption'];
            $video->attach_file($_FILES['file_upload']);
            if($video->save()){
                $this->message = "Video upload successfully.";
                $this->oLogger->log_action("Upload", "UserID ". $this->aUser->id ."| Uploaded a file (".$video->filename.")!");
                redirect_to('index.php?cl=videolist&fnc=showAll');
            }else{
                $message = join("<br />", $video->errors);
            }
        }else{
            $this->sTemplateTpl = 'upload_video.tpl';
            $this->addTplParam( 'max_file_size', $max_file_size );
        }
    }

    public function showComments()
    {
        if(!isset($_GET['vid_id'])){
            $this->message = "No video ID was provided.";
            redirect_to("index.php?cl=videolist&fnc=showAll");
        }

        $this->sTemplateTpl = 'comments.tpl';
        $video = Video::find_by_id($_GET['vid_id']);
        $comments = $video->comments();

        if(!$video){
            $this->message = "No video was found.";
            redirect_to("index.php?cl=videolist&fnc=showAll");
        }else{
            $this->addTplParam( 'video', $video );
            $this->addTplParam( 'comments', $comments );
            $this->addTplParam( 'type', 'video' );
        }
    }

    public function deleteComment()
    {
        if(!isset($_GET['id'])){
            $this->message = "No comment ID was provided.";
            redirect_to('index.php?cl=homeback');
        }

        $comment = Comment::find_by_id($_GET['id']);
        if($comment && $comment->delete()){
            $this->message = "The comment was deleted.";
            $this->oLogger->log_action("Delete", "UserID ". $this->aUser->id ." | Deleted a comment!");
            redirect_to("index.php?cl=videolist&fnc=showComments&vid_id=".$_GET['vid_id']);
        }else{
            $this->message = "The comment could not be deleted.";
            redirect_to('index.php?cl=videolist&fnc=showAll');
        }
    }

    public function render()
    {
        $this->addTplParam( 'videos', $this->videos );
        $this->addTplParam( 'message', $this->message );
        $this->addTplParam( 'aUser', $this->aUser );
        return parent::render();
    }
}