<?php

class Logfile extends BackendController{
    protected $sTemplateTpl = 'log.tpl';

    private $logger;
    private $log;
    private $message;

    public function init()
    {
        $this->logger = new Logger();
        return parent::init();
    }

    public function show()
    {
        $this->log = $this->logger->read_log();
    }

    public function clear()
    {
        if(isset($_POST['clear'])){
            $session = Registry::make('Session');
            $this->message = $this->logger->clear_log($session->user_id);
            $this->log = $this->logger->read_log();
        }
    }

    public function render()
    {
        $this->addTplParam( 'log', $this->log );
        $this->addTplParam( 'message', $this->message );
        return parent::render();
    }
}