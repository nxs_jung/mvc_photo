<?php

class userManagement extends BackendController{

    protected $sTemplateTpl = 'userManagement.tpl';

    private $aUser;

    public function init(){
        $session = Registry::make('Session');

        $this->aUser = User::find_by_id($session->user_id);
        if($_GET['fnc'] == "showAll" && !$session->role->authorize_user()){
            redirect_to('index.php?cl=HomeBack');
        }else{
            return parent::init();
        }
    }

    public function showAll(){
        $session = Registry::make('Session');

        if($session->role->authorize_user()){
            $user_set = User::find_all();
            $this->addTplParam( 'user_set', $user_set );
            $this->sTemplateTpl = 'userManagement.tpl';
        }
    }

    public function show_by_id($id = 0)
    {
        if(isset($_GET['id']) || $id != 0){
            if(isset($_GET['id']) && $id == 0){
                $id = $_GET['id'];
            }

            $user = User::find_by_id($id);
            if($user instanceOf User){
                $user->init_role();
                $user->init_adresse();
                $this->addTplParam( 'user', $user );
                $this->sTemplateTpl = 'user.tpl';
            }
        }
    }

    public function update()
    {
        if($_POST['submit']){
            $id = $_GET['id'];
            $user = User::find_by_id($id);
            $user->init_role();

            $n_pw = $_POST['pw'];
            $n_mail = $_POST['mail'];

            if($user->admin != 2){
                $create_vid = $_POST['create_vid'];
                $create_pho = $_POST['create_pho'];
                $create_user = $_POST['create_user'];
            }

            $street = $_POST['street'];
            $number = $_POST['number'];
            $plz = $_POST['plz'];
            $ort = $_POST['ort'];
            $country = $_POST['country'];

            if (isset($_POST['admin'])) {
                $admin = $_POST['admin'];
            }

            $message ="";

            $pw_ch = false;
            $m_ch = false;

            if($n_pw !== $user->password && isset($_POST['pw_re']) && $n_pw === $_POST['pw_re']){
                $user->password = $n_pw;
                $message = "The password was changed!";
                $pw_ch= true;
            }elseif($n_pw !== $user->password && isset($_POST['pw_re'])) {
                $message = "The passwords were not equal!";
            }elseif($n_mail !== $user->mail){
                $m_ch = $user->set_mail($n_mail);
                $message .= ($m_ch) ? "The Mail was successfully changed." : "An Error occured.";
            }

            if($user->admin != 2){
                $message .= $user->set_role($create_pho, $create_vid, $create_user);
            }

            if(isset($admin) && $admin !== $user->admin){
                $user->admin = $admin;
            }

            if( isset($street) && $street != "" &&
                isset($number) && $number != "" &&
                isset($plz) && $plz != "" &&
                isset($ort) && $ort != "" &&
                isset($country) && $country != "" ){
                $user->set_adresse($street, $number, $plz, $ort, $country);
            }

            if($user->save()){
                $message .= "The user was updated!";
                $this->oLogger->log_action("Edit User", "UserID ". $this->aUser->id ." | ". $user->username." has been edited!");
                $this->addTplParam( 'user', $user );
                $this->addTplParam( 'message', $message );
            }
        }
        if(isset($_GET['id'])){
            $this->show_by_id($id);
        }else{
            $this->show_by_id();
        }
    }

    public function delete(){
        if(isset($_GET['id'])){
            $id = $_GET['id'];
            $user = User::find_by_id($id);
            if($user instanceOf User){
                $user->delete();
                $this->oLogger->log_action("Delete User", "UserID ". $this->aUser->id ." | The User ". $user->username." has been deleted!");
            }
            $this->showAll();
        }
    }

    public function create()
    {
        if(isset($_POST['submit'])){
            $user = new User();
            $user->rRights = new Role();
            $errors = array();

            $username = $_POST['username'];
            $first_name = $_POST['first_name'];
            $last_name = $_POST['last_name'];
            $mail = $_POST['mail'];
            $create_vid = $_POST['create_vid'];
            $create_pho = $_POST['create_pho'];
            $create_user = $_POST['create_user'];
            $admin = $_POST['admin'];

            $message = "";

            $validator = new User_Validator($user);
            $errors = $validator->validate($_POST);

            if(count($errors) < 1 && $user->save()){
                $this->oLogger->log_action("Create User", "UserID ". $this->aUser->id ." | The User ". $user->full_name()." (".$user->username.") has been created!");
                redirect_to("index.php?cl=userManagement&fnc=showAll");
            }else{
                foreach ($errors as $msg) {
                    $message .= $msg."<br>";
                }
                $this->sTemplateTpl = 'userCreate.tpl';
                $this->addTplParam( 'message', $message );
            }
        }else{
            $username = "";
            $first_name = "";
            $last_name = "";
            $mail ="";
            $this->sTemplateTpl = 'userCreate.tpl';
        }
    $this->addTplParam( 'username', $username );
    $this->addTplParam( 'first_name', $first_name );
    $this->addTplParam( 'last_name', $last_name );
    $this->addTplParam( 'mail', $mail );
    }

    public function render()
    {
        $session = Registry::make('Session');

        $this->addTplParam( 'session', $session );
        return parent::render();
    }
}
