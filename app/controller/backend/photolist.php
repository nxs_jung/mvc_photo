<?php

class PhotoList extends BackendController{

    protected $sTemplateTpl = 'photo_list.tpl';

    private $photos;
    private $aUser;
    private $message;

    public function init()
    {
        $session = Registry::make('Session');
        $this->aUser = User::find_by_id($session->user_id);
        return parent::init();
    }

    public function showAll(){
        $this->sTemplateTpl = 'photo_list.tpl';
        $this->photos = Photograph::find_all();
        if(!$this->photos){
            redirect_to('index.php?cl=homeback');
        }
    }

    public function delete()
    {
        if(!isset($_GET['id'])){
            $this->message = "No photo ID was provided.";
        }
        elseif( ($photo = Photograph::find_by_id($_GET['id']) ) && $photo->destroy() )
        {
            $this->message = "The photo was deleted.";
            $this->oLogger->log_action("Delete", "UserID ". $this->aUser->id ."| Deleted a file (".$photo->filename.") !");
        }
        else
        {
            $this->message = "The photo could not be deleted.";
        }

        redirect_to('index.php?cl=photolist&fnc=showAll');
    }

    public function create()
    {
        if(isset($_POST['submit'])){
            $photo = new Photograph();
            $photo->caption = $_POST['caption'];
            $photo->attach_file($_FILES['file_upload']);
            if($photo->save()){
                $this->message = "Photograph upload successfully.";
                $this->oLogger->log_action("Upload", "UserID ". $this->aUser->id ."| Uploaded a file (".$photo->filename.")!");
                redirect_to('index.php?cl=photolist&fnc=showAll');
            }else{
                $message = join("<br />", $photo->errors);
                $this->sTemplateTpl = 'upload_photo.tpl';
                $this->addTplParam( 'message', $message );
            }
        }else{
            $this->sTemplateTpl = 'upload_photo.tpl';
        }
    }

    public function showComments()
    {
        if(!isset($_GET['pho_id'])){
            $this->message = "No photo ID was provided.";
            redirect_to("index.php?cl=photolist&fnc=showAll");
        }

        $this->sTemplateTpl = 'comments.tpl';
        $photo = Photograph::find_by_id($_GET['pho_id']);
        $comments = $photo->comments();

        if(!$photo){
            $this->message = "No photo was found.";
            redirect_to("index.php?cl=photolist&fnc=showAll");
        }else{
            $this->addTplParam( 'photo', $photo );
            $this->addTplParam( 'comments', $comments );
            $this->addTplParam( 'type', 'photo' );
        }
    }

    public function deleteComment()
    {
        if(!isset($_GET['id'])){
            $this->message = "No comment ID was provided.";
            redirect_to('index.php?cl=homeback');
        }

        $comment = Comment::find_by_id($_GET['id']);
        if($comment && $comment->delete()){
            $this->message = "The comment was deleted.";
            $this->oLogger->log_action("Delete", "UserID ". $this->aUser->id ." | Deleted a comment!");
            redirect_to("index.php?cl=photolist&fnc=showComments&pho_id=".$_GET['pho_id']);
        }else{
            $this->message = "The comment could not be deleted.";
            redirect_to('index.php?cl=photolist&fnc=showAll');
        }
    }

    public function render()
    {
        $this->addTplParam( 'photos', $this->photos );
        $this->addTplParam( 'is_admin', $this->aUser->authorize_admin() );
        return parent::render();
    }
}