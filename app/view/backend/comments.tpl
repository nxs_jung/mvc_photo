{include '../layout/header_b.tpl'}

<h2>Comments on {if isset($photo->filename)} {$photo->filename} {else} {$video->filename} {/if}</h2>
{if isset($message)}{$message}{/if}
<p>
    <a href="{if $type == 'photo'}photos.html{else}videos.html{/if}">
        &laquo; Back
    </a>
</p>

<br />

{foreach $comments as $comment}
    <div class="comment" style="margin-bottom: 2em;">
        <div class="author">
            {$comment->author} wrote:
        </div>
        <div class="body">
            {$comment->body}
        </div>
        <div class="meta-info">
            {$comment->created}
        </div>
        <div class="actions" style="font-size: 0.8em;">
            <a href="index.php?cl={if $type == 'photo'}photolist{else}videolist{/if}&fnc=deleteComment&id={$comment->id}&{if $type == 'photo'}pho_id{else}vid_id{/if}={if $type == 'photo'}{$photo->id}{else}{$video->id}{/if}">
                Delete Comment
            </a>
        </div>
    </div>
{/foreach}
{if empty($comments)}No Comments!{/if}

{include '../layout/footer_b.tpl'}