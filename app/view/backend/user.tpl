{include '../layout/header_b.tpl'}

<h2>
    User edit of {$user->full_name()} | {$user->username}
</h2>
{if isset($message)}{$message}{/if}

<form action="index.php?cl=userManagement&fnc=update&id={$user->id}" method="POST">
    <p style="border: 1px solid black; text-align: center; width: 400px; padding: 10px;">
        Username: {$user->username}
    </p>
    <p style="border: 1px solid black; text-align: center; width: 400px; padding: 10px;">
        Password: <input type="password" name="pw" value="{$user->password}"><br/>
        Password wiederholen: <input type="password" name="pw_re" /><br/><br/>
        Zum &auml;ndern des Passwortes ein neues Passwort eingeben und wiederholen!
    </p>
    <p style="border: 1px solid black; text-align: center; width: 400px; padding: 10px;">
        Email-Adresse: <input type="text" name="mail" value="{$user->mail}">
    </p>

    {if $session->role->authorize_user() && $user->authorize_admin() != 2}
    <div style="border: 1px solid black; text-align: center; width: 400px; padding: 10px; -webkit-margin-before: 1em; -webkit-margin-after: 1em;">
        <p>
            Videos hochladen:
            <select name="create_vid" >
                <option value="1" {if $user->rRights->authorize_video()}selected{/if} >Ja</option>
                <option value="0" {if !$user->rRights->authorize_video()}selected{/if} >Nein</option>
            </select>
        </p>
        <p>
            Bilder hochladen:
            <select name="create_pho" >
                <option value="1" {if $user->rRights->authorize_photo()}selected{/if} >Ja</option>
                <option value="0" {if !$user->rRights->authorize_photo()}selected{/if} >Nein</option>
            </select>
        </p>
        <p>
            User editieren:
            <select name="create_user" >
                <option value="1" {if $user->rRights->authorize_user()}selected{/if} >Ja</option>
                <option value="0" {if !$user->rRights->authorize_user()}selected{/if} >Nein</option>
            </select>
        </p>
        {if $user->authorize_admin()}
            <p>
                Admin:
                <select name="admin" >
                    <option value="1" {if $user->authorize_admin()}selected{/if} >Ja</option>
                    <option value="0" {if !$user->authorize_admin()}selected{/if} >Nein</option>
                </select>
            </p>
        {/if}
        </p>
    </div>
    {/if}

    <div style="border: 1px solid black; text-align: center; width: 400px; padding: 10px;">
        <p>
            Straße + Hausnummer: <input type="text" name="street" value="{if isset($user->aAdresse->street)}{$user->aAdresse->street}{/if}" />
                                 <input type="text" name="number" value="{if isset($user->aAdresse->number)}{$user->aAdresse->number}{/if}"
                                 style="width: 30px;" />
        </p>
        <p>
            PLZ + Ort: <input type="text" name="plz" value="{if isset($user->aAdresse->plz)}{$user->aAdresse->plz}{/if}" style="width: 50px;" />
                       <input type="text" name="ort" value="{if isset($user->aAdresse->ort)}{$user->aAdresse->ort}{/if}"/>
        </p>
        <p>
            Land: <input type="text" name="country" value="{if isset($user->aAdresse->country)}{$user->aAdresse->country}{/if}"/>
        </p>
    </div>

    <p>
        <input type="submit" name="submit" value="Update!" />
    </p>
</form>
<br>
<a href="user.html">Zur&uuml;ck</a>
{include '../layout/footer_b.tpl'}
