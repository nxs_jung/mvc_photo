{include '../layout/header_b.tpl'}

<h2>User creation</h2>
{if isset($message)}{$message}{/if}

<form action="index.php?cl=userManagement&fnc=create" method="POST">
    <div>
        <p>
            Username: <input type="text" name="username" value="{if isset($username)}{$username}{/if}" />
        </p>
        <p>
            Password: <input type="password" name="password" value=""/>
        </p>
        <p>
            First name: <input type="text" name="first_name" value="{if isset($first_name)}{$first_name}{/if}"/>
        </p>
        <p>
            Last name: <input type="text" name="last_name" value="{if isset($last_name)}{$last_name}{/if}"/>
        </p>
        <p>
            Mail: <input type="text" name="mail" value="{if isset($mail)}{$mail}{/if}"/>
        </p>
        <p>
            Soll der User Videos hochladen k&ouml;nnen?<br/>
            <select name="create_vid" >
                <option value="1">Ja</option>
                <option value="0">Nein</option>
            </select>
        </p>
        <p>
            Soll der User Bilder hochladen k&ouml;nnen?<br/>
            <select name="create_pho" >
                <option value="1">Ja</option>
                <option value="0">Nein</option>
            </select>
        </p>
        <p>
            Soll der User andere User bearbeiten k&ouml;nnen?<br/>
            <select name="create_user" >
                <option value="0">Nein</option>
                <option value="1">Ja</option>
            </select>
        </p>
        <p>
            Ist der User ein admin?<br/>
            <select name="admin" >
                <option value="0">Nein</option>
                <option value="1">Ja</option>
            </select>
        </p>
    </div>
    <div>
        <p>
            Straße + Hausnummer: <input type="text" name="street" value="{if isset($street)}{$street}{/if}" />
                                 <input type="text" name="number" value="{if isset($number)}{$number}{/if}" />
        </p>
        <p>
            PLZ + Ort: <input type="text" name="plz" value="{if isset($plz)}{$plz}{/if}"/>
                       <input type="text" name="ort" value="{if isset($ort)}{$ort}{/if}"/>
        </p>
        <p>
            Land: <input type="text" name="country" value="{if isset($country)}{$country}{/if}"/>
        </p>
    </div>
    <p>
        <input type="submit" name="submit" value="Erstellen" />
    </p>
</form>
<br>
<a href="user.html">Zur&uuml;ck</a>
{include '../layout/footer_b.tpl'}
