{include '../layout/header_b.tpl'}

<h2>Videos</h2>
{if isset($message)}{$message}{/if}

<table class="bordered">
    <tr>
        <th>Video</th>
        <th>Filename</th>
        <th>Caption</th>
        <th>Size</th>
        <th>Type</th>
        <th>Comments</th>
        {if $aUser->authorize_admin()}
            <th>&nbsp;</th>
        {/if}
    </tr>
    {foreach $videos as $video}
    <tr>
        <td><img src="../{$video->image_path()}" width="100" /></td>
        <td>{$video->filename}</td>
        <td>{$video->caption}</td>
        <td>{$video->size_as_text()}</td>
        <td>{$video->type}</td>
        <td>
            <a href="index.php?cl=videolist&fnc=showComments&vid_id={$video->id}">
                {count($video->comments())}
            </a>
        </td>
        {if $aUser->authorize_admin()}
            <td><a href="index.php?cl=photolist&fnc=delete&id={$video->id}">Delete</a></td>
        {/if}
    </tr>
    {/foreach}
</table>


<br />
+ <a href="index.php?cl=videolist&fnc=create">Upload a new video</a><br />
<a href="admin.html">Zur&uuml;ck</a>

{include '../layout/footer_b.tpl'}