{include '../layout/header_b.tpl'}

<h2>Users of the Photogallery</h2>

{if isset($user_set)}
<ul>

    {foreach $user_set as $user}
        <li id="{$user->id}">{$user->username} | {$user->full_name()} <a href="index.php?cl=userManagement&fnc=show_by_id&id={$user->id}">edit</a>{if $user->admin === "2"} {else} | <a href="index.php?cl=userManagement&fnc=delete&id={$user->id}">delete</a></li>{/if}
    {/foreach}
</ul>

{else}
    No User available.
{/if}
<br>
<a href="index.php?cl=userManagement&fnc=create">User erstellen!</a>
<br>
<br>
<a href="admin.html">Zur&uuml;ck</a>
{include '../layout/footer_b.tpl'}