{include '../layout/header_b.tpl'}

<h2>Photographs</h2>
{if isset($message)}{$message}{/if}

<table class="bordered">
    <tr>
        <th>Image</th>
        <th>Filename</th>
        <th>Caption</th>
        <th>Size</th>
        <th>Type</th>
        <th>Comments</th>
        {if $is_admin}
            <th>&nbsp;</th>
        {/if}
    </tr>
    {foreach $photos as $photo}
    <tr>
        <td><img src="../{$photo->image_path()}" width="100" /></td>
        <td>{$photo->filename}</td>
        <td>{$photo->caption}</td>
        <td>{$photo->size_as_text()}</td>
        <td>{$photo->type}</td>
        <td>
            <a href="index.php?cl=photolist&fnc=showComments&pho_id={$photo->id}">
                {count($photo->comments())}
            </a>
        </td>
        {if $is_admin}
            <td><a href="index.php?cl=photolist&fnc=delete&id={$photo->id}">Delete</a></td>
        {/if}
    </tr>
    {/foreach}
</table>


<br />
+ <a href="index.php?cl=photolist&fnc=create">Upload a new photograph</a><br />
<a href="admin.html">Zur&uuml;ck</a>

{include '../layout/footer_b.tpl'}