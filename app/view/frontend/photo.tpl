{include '../layout/header_f.tpl'}
<a href="index.php">&laquo; Back</a><br /><br />
{if isset($message)}{$message}{/if}

<div style="margin-left: 20px;">
    <img src="{$photo->image_path()}" alt="{$photo->filename}">
    <p>{$photo->caption}</p>
</div>

<div id="comments">
    {foreach $comments as $comment}
        <div class="comment" style="margin-bottom: 2em;">
            <div class="author">
                {$comment->author} wrote:
            </div>
            <div class="body">
                {$comment->body}
            </div>
            <div class="meta-info">
                {$comment->created}
            </div>
        </div>
    {/foreach}
    {if empty($comments)}No Comments!{/if}
</div>

<div id="comment-form">
    <h3>New Comment</h3>
    <form action="index.php?cl=photo&fnc=comment&id={$photo->id}" method="POST">
        <table>
            <tr>
                <td>Your Name:</td>
                <td>
                    <input type="text" name="author" value="{if isset($author)}{$author}{/if}{if isset($user->username)}{$user->username}{/if}"
                    {if isset($user->username)} disabled="disabled"{/if}>
                </td>
            </tr>
            <tr>
                <td>Your Comment:</td>
                <td>
                    <textarea name="body"  cols="40" rows="8">{if isset($body)}{$body}{/if}</textarea>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input type="submit" name="submit" value="Submit Comment" />
                </td>
            </tr>
        </table>
    </form>
</div>

{include '../layout/footer_f.tpl'}