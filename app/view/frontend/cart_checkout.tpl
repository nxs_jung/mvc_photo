
{include '../layout/header_f.tpl'}

<h2>
    Anschrift von {if isset($user)}{$user->full_name()}{else} ... {/if}
</h2>
{if isset($message)}{$message}{/if}

<div>
    {$cart}
</div>

<form action="index.php?cl=shop&fnc=checkout&id={$user->id}" method="POST">

    <p>
        <input type="submit" name="submit" value="Bestellung abschicken!" />
    </p>

    <p style="border: 1px solid black; text-align: center; width: 400px; padding: 10px;">
        Email-Adresse: <input type="text" name="mail" value="{if isset($user->mail)}{$user->mail}{/if}">
    </p>

    <div style="border: 1px solid black; text-align: center; width: 400px; padding: 10px;">
        <p>
            Straße + Hausnummer: <input type="text" name="street" value="{if isset($user->aAdresse->street)}{$user->aAdresse->street}{/if}" />
                                 <input type="text" name="number" value="{if isset($user->aAdresse->number)}{$user->aAdresse->number}{/if}"
                                 style="width: 30px;" />
        </p>
        <p>
            PLZ + Ort: <input type="text" name="plz" value="{if isset($user->aAdresse->plz)}{$user->aAdresse->plz}{/if}" style="width: 50px;" />
                       <input type="text" name="ort" value="{if isset($user->aAdresse->ort)}{$user->aAdresse->ort}{/if}"/>
        </p>
        <p>
            Land: <input type="text" name="country" value="{if isset($user->aAdresse->country)}{$user->aAdresse->country}{/if}"/>
        </p>
    </div>

    {if $debit}
    <p>
        Debit Card Number: <input type="text" name="debit_nr" />
    </p>
    {elseif $iban}
    <p>
        IBAN: <input type="text" name="iban_nr" />
        BIC: <input type="text" name="bic_nr" />
    </p>
    {/if}
</form>

<form action="index.php?cl=shop&fnc=set_payment" method="POST" class="payment">
    <p>
        Zahlungsart: <select name="payment" class="payment" id="payment">
                        <option value="">...</option>
                        <option value="debit" {if $debit}selected{/if} >Kreditkarte</option>
                        <option value="iban" {if $iban}selected{/if} >Bankeinzug</option>
                     </select>
    </p>
</form>

{literal}
<script type="text/javascript">
    $(function() {
        $('#payment').change(function() {
            console.log("The value changed!");
            this.form.submit();
    });
});
</script>
{/literal}
<br>




<a href="index.html">Zur&uuml;ck</a>
{include '../layout/footer_f.tpl'}