{include '../layout/header_f.tpl'}
<p>
    {if !$logged_in}<a href="login.html"><button>Login</button></a>{else}<a href="admin.html"><button>Backend</button></a>{/if}
</p>
<p>
    <a href="index.php?cl=shop&fnc=show">Einkaufskorb</a>
</p>

<div class="photo-wrapper">
    {foreach $photos as $photo}
    <div style="float: left; margin-left: 20px; border: 1px solid grey; padding: 3px;margin: 3px;">
        <a href="index.php?cl=photo&fnc=show&id={$photo->id}">
            <img src="{$photo->image_path()}" alt="{$photo->filename}" width="200">
        </a>
        <p>{$photo->caption}  -  <a href="index.php?cl=shop&fnc=add_to_cart&id={$photo->id}&type=photo"> Add to Cart </a></p>
    </div>

    {/foreach}
</div>

<div class="video-wrapper">
    {foreach $videos as $video}
    <div style="float: left; margin-left: 20px; border: 1px solid grey; padding: 3px;margin: 3px;">
        <a href="index.php?cl=videocontroller&fnc=show&id={$video->id}">
            <img src="{$video->image_path()}" alt="{$video->filename}" width="200">
        </a>
        <p>{$video->caption}  -  <a href="index.php?cl=shop&fnc=add_to_cart&id={$video->id}&type=video"> Add to Cart </a></p>
    </div>
    {/foreach}
</div>

{include '../layout/footer_f.tpl'}