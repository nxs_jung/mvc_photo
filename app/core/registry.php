<?php
/**
 * This Software is the property of NEXUS Netsoft GmbH
 * and is protected by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license
 * key is a violation of the license agreement and will be
 * prosecuted by civil and criminal law.
 *
 * Alle Rechte vorbehalten
 *
 * @package   NXS_Modules
 * @author    Rafal Wesolowski <wesolowski@nexus-netsoft.com>
 * @copyright (C) 2013, NEXUS Netsoft GmbH
 * @see       http://www.nexus-netsoft.com
 */

/**
 *  IoC Container
 *
 * @category   IoC
 * @author     Rafal Wesolowski <wesolowski@nexus-netsoft.com>
 * @version    1.0
 */
class Registry
{
    /**
     * Array mit Objects / Closuer
     *
     * @var array
     */
    public static $oRegistry = array();

    /**
    * Add a new resolver to the registry array.
    *
    * @author Rafal Wesolowski <wesolowski@nexus-netsoft.com>
    * @param  string $sName The id
    * @param  object $oObject Object that creates instance
    * @return void
    */
    public static function register( $sName, $oObject )
    {
        static::$oRegistry[$sName] = $oObject;
    }

    /**
    *  Lifert Reigister von IoC, wenn nicht exist und das ein Class ist dann
    *  erstellt und gib die zur�ck
    *
    * @author Rafal Wesolowski <wesolowski@nexus-netsoft.com>
    * @param  string $sName The id
    * @throws Exception If Class oder Reigister nicht gefunden ist
    * @return object
    */
    public static function make( $sName )
    {
        $bIsRegistered = static::isRegistered( $sName );
        if( $bIsRegistered )
        {
            $oObject = static::$oRegistry[$sName];
        }
        else if ( !$bIsRegistered && class_exists( $sName, true ) )
        {
            $oObject = new $sName();
            static::register( $sName, $oObject );
        }
        else
        {
            $oObject = null;
            throw new Exception("No registered resolver for {$name} in the IoC");
        }
        return $oObject;
    }

    /**
    * Determine whether the id is registered
    *
    * @author Rafal Wesolowski <wesolowski@nexus-netsoft.com>
    * @param  string $sName The id
    * @return bool Whether to id exists or not
    */
    public static function isRegistered( $sName )
    {
        return array_key_exists( $sName, static::$oRegistry);
    }

    /**
    * Lifert Reigister von IoC
    *
    * @author Rafal Wesolowski <wesolowski@nexus-netsoft.com>
    * @param  string $sName ClassName
    * @throws Exception If Class oder Reigister nicht gefunden ist
    * @return object
    */
    public static function get( $sName )
    {
        if( static::isRegistered( $sName ) )
        {
            $oObject = static::$oRegistry[$sName];
        }
        else
        {
            $oObject = null;
            throw new Exception("No registered resolver for {$name} in the IoC");
        }
        return $oObject;
    }

    /**
    * Löcht das Eintrag von IoC Container
    *
    * @author Rafal Wesolowski <wesolowski@nexus-netsoft.com>
    * @param  string $sName ClassName
    * @return void
    */
    public static function delete( $sName )
    {
        if( static::isRegistered( $sName ) )
        {
            unset( static::$oRegistry[$sName] );
        }

    }

}
