<?php

class BackendController extends Controller{
    public function init()
    {
        $session = Registry::make('Session');

        if($session->is_logged_in()){
            $user = User::find_by_id($session->user_id);
        }
        if(isset($user) && $user instanceOf User){
            $this->oLoad->area = "backend";
        }else{
            redirect_to('index.php?cl=login');
        }
    }
}