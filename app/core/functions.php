<?php

/**
 * Kürzt die Nullen aus einem Datum
 *
 * @author Moritz Jung <jung@nexus-netsoft.com>
 * @param  string $marked_string der gehighlightete String
 * @return String
 */
function strip_zeros_from_date($marked_string=""){
    // first remove marked zeros
    $no_zeros = str_replace('*0', '', $marked_string);
    // then remove any remaining marks
    $cleaned_string = str_replace('*', '', $no_zeros);
    return $cleaned_string;
}

/**
 * Leitet auf eine andere Seite weiter
 *
 * @author Moritz Jung <jung@nexus-netsoft.com>
 * @param  String $location
 * @return void
 */
function redirect_to ($location = NULL){
    if($location != NULL){
        header("Location: ".$location);
        exit;
    }
}

/**
 * Zeigt eine Fehlermeldung/Nachricht/Warnung an
 *
 * @author Moritz Jung <jung@nexus-netsoft.com>
 * @param  string $message Meldung
 * @return String
 */
function output_message($message=""){
    if(!empty($message)){
        return "<p class\"message\">".nl2br($message)."</p>";
    }else{
        return "";
    }
}

//----------------------------------------------------------------------------------//

/**
 * Lädt vergessene Script-Dateien bzw Includes
 *
 * @author Moritz Jung <jung@nexus-netsoft.com>
 * @param  String $class_name Klassenname der vegessenen include-Datei
 * @return void
 */
function __autoload($class_name){
    $class_name = strtolower($class_name);
    $path = LIB_PATH.DS.$class_name.".php";
    if(file_exists($path)){
        require_once($path);
    }else{
        die("The file ". $class_name .".php could not be found.");
    }
}

/**
 * Included ein angegebenes Layout
 *
 * @author Moritz Jung <jung@nexus-netsoft.com>
 * @param  string $template Layout-TMP
 * @return void
 */
function include_layout_template($template=""){
    include (SITE_ROOT.DS.'public'.DS.'layouts'.DS.$template);
}

//----------------------------------------------------------------------------------//

/**
 * Wandelt ein Datum in Klartext um
 *
 * @author Moritz Jung <jung@nexus-netsoft.com>
 * @param  string $datetime SQL-Zeitformat
 * @return String
 */
function datetime_to_text($datetime=""){
    $unixdatetime = strtotime($datetime);
    return strftime("%B %d, %Y at %I:%M %p", $unixdatetime);
}
