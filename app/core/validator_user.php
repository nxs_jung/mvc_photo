<?php

class User_Validator extends Validator {

    public function __construct(User $object) {
        $this->object = $object;
    }

    public function validate(Array $post = null){
        $aInfo = array('first_name', 'last_name', 'password', 'admin');
        foreach ($aInfo as $name)
        {
            $value = $post[$name];
            if(isset($value) && $value !== ""){
                $this->object->$name = $value;
            }else{
                $this->occured_errors[] = $this->errors[$name];
            }
        }

        if(isset($post['username']) && $post['username'] !== "" && User::find_by_username($post['username']) === NULL){
            $this->object->username = $post['username'];
        }else{
            $this->occured_errors[] = $this->errors['username'];
        }

        $this->object->set_role($post['create_vid'], $post['create_pho'], $post['create_user']);

        if (isset($post['mail']) && $post['mail'] !== '' && !$this->object->set_mail($post['mail'])) {
            $this->occured_errors[] = $this->errors["mail"];
        }elseif(!isset($post['mail']) || $post['mail'] == ''){
            $this->occured_errors[] = $this->errors["no_mail"];
        }else{
            $this->object->set_mail($post['mail']);
        }
        return $this->occured_errors;
    }

}