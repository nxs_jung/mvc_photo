<?php

/**
 * Pattern for Controller
 *
 * @author     Rafal Wesolowski <wesolowski@nexus-netsoft.com>
 * @version    1.0
 */
abstract class Controller {

    /**
     * Load Class
     *
     * @var object
     */
    protected $oLoad = null;

    protected $oLogger = null;


    /**
     * Class constructor, sets oLoad and oProductRepository.
     *
     * @author Rafal Wesolowski <wesolowski@nexus-netsoft.com>
     */
    public function __construct()
    {
        $this->oLoad = new Load();
        $this->oLogger = new Logger();
    }



    public function init()
    {

    }



    /**
     * Render Function
     *
     * @author Rafal Wesolowski <wesolowski@nexus-netsoft.com>
     * @param  string $sPage Pagename
     * @return void
     */
    public function render()
    {
        $this->oLoad->render( $this->sTemplateTpl );
    }

    /**
     * Getter for aData
     *
     * @author Rafal Wesolowski <wesolowski@nexus-netsoft.com>
     * @param  string $sParamName parameter name
     * @param  mixed  $mValue     parameter value
     * @return void
     */
    public function addTplParam( $sParamName, $mValue )
    {
        $this->oLoad->addTplParam( $sParamName, $mValue );
    }
}