<?php

class Payment_Validator extends Validator {

    public function __construct() {
    }

    public function validate_pay(Array $post = null){
        $aInfo = array('debit_nr', 'iban_nr', 'bic_nr');
        foreach ($aInfo as $name)
        {
            $value = $post[$name];

            $payment[$name] = $value;
            if($name == "debit_nr" && isset($value) && $value != ""){
                $debit_valid = $this->luhn_algo($value);
            }elseif ($name == "iban_nr" && isset($value) && $value != "") {
                $iban_valid = $this->checkIBAN($value);
            }elseif ($name == "bic_nr" && isset($value) && $value != "") {
                $bic_valid = $this->validateBIC($value);
            }
        }

        if(isset($debit_valid) && $debit_valid){
            $aMessages['debit'] = true;
        }elseif(isset($debit_valid)){
            $aMessages['debit'] = "Die Kreditkartennummer ist nicht korrekt.";
        }

        if(isset($iban_valid) && $iban_valid && isset($bic_valid) && $bic_valid){
            $aMessages['iban'] = true;
            $aMessages['bic'] = true;
        }elseif(isset($iban_valid) && $iban_valid && isset($bic_valid)){
            $aMessages['bic'] = "Die BIC ist nicht korrekt.";
        }elseif(isset($bic_valid) && $bic_valid && isset($iban_valid)){
            $aMessages['iban'] = "Die IBAN ist nicht korrekt.";
        }else{
            $aMessages['bic'] = "Die BIC ist nicht korrekt.";
            $aMessages['iban'] = "Die IBAN ist nicht korrekt.";
        }

        return $aMessages;
    }

    function checkIBAN($iban)
    {
        $iban = strtolower(str_replace(' ','',$iban));
        $Countries = array('al'=>28,
                    'ad'=>24,
                    'at'=>20,
                    'az'=>28,
                    'bh'=>22,
                    'be'=>16,
                    'ba'=>20,
                    'br'=>29,
                    'bg'=>22,
                    'cr'=>21,
                    'hr'=>21,
                    'cy'=>28,
                    'cz'=>24,
                    'dk'=>18,
                    'do'=>28,
                    'ee'=>20,
                    'fo'=>18,
                    'fi'=>18,
                    'fr'=>27,
                    'ge'=>22,
                    'de'=>22,
                    'gi'=>23,
                    'gr'=>27,
                    'gl'=>18,
                    'gt'=>28,
                    'hu'=>28,
                    'is'=>26,
                    'ie'=>22,
                    'il'=>23,
                    'it'=>27,
                    'jo'=>30,
                    'kz'=>20,
                    'kw'=>30,
                    'lv'=>21,
                    'lb'=>28,
                    'li'=>21,
                    'lt'=>20,
                    'lu'=>20,
                    'mk'=>19,
                    'mt'=>31,
                    'mr'=>27,
                    'mu'=>30,
                    'mc'=>27,
                    'md'=>24,
                    'me'=>22,
                    'nl'=>18,
                    'no'=>15,
                    'pk'=>24,
                    'ps'=>29,
                    'pl'=>28,
                    'pt'=>25,
                    'qa'=>29,
                    'ro'=>24,
                    'sm'=>27,
                    'sa'=>24,
                    'rs'=>22,
                    'sk'=>24,
                    'si'=>19,
                    'es'=>24,
                    'se'=>24,
                    'ch'=>21,
                    'tn'=>24,
                    'tr'=>26,
                    'ae'=>23,
                    'gb'=>22,
                    'vg'=>24);

        $Chars = array('a'=>10,
                'b'=>11,
                'c'=>12,
                'd'=>13,
                'e'=>14,
                'f'=>15,
                'g'=>16,
                'h'=>17,
                'i'=>18,
                'j'=>19,
                'k'=>20,
                'l'=>21,
                'm'=>22,
                'n'=>23,
                'o'=>24,
                'p'=>25,
                'q'=>26,
                'r'=>27,
                's'=>28,
                't'=>29,
                'u'=>30,
                'v'=>31,
                'w'=>32,
                'x'=>33,
                'y'=>34,
                'z'=>35);

        if(strlen($iban) == $Countries[substr($iban,0,2)]){

            $MovedChar = substr($iban, 4).substr($iban,0,4);
            $MovedCharArray = str_split($MovedChar);
            $NewString = "";

            foreach($MovedCharArray AS $key => $value){
                if(!is_numeric($MovedCharArray[$key])){
                    $MovedCharArray[$key] = $Chars[$MovedCharArray[$key]];
                }
                $NewString .= $MovedCharArray[$key];
            }

            if(bcmod($NewString, '97') == 1)
            {
                return TRUE;
            }
            else{
                return FALSE;
            }
        }
        else{
            return FALSE;
        }
    }

    function validateBIC($code)
    {
        return(eregi("^([a-zA-Z]){4}([a-zA-Z]){2}([0-9a-zA-Z]){2}([0-9a-zA-Z]{3})?$", $code));
    }


    public function luhn_algo($debit_nr)
    {
        $debit_nr = strrev($debit_nr);
        $debit_nr = (int) $debit_nr;
        $aDebit_nr = str_split($debit_nr, 1);

        foreach ($aDebit_nr as $pos => $zahl) {
            if($pos%2 == 0 || $pos == 0){
                $out .= $this->luhn($zahl);
            }else{
                $out .= $zahl;
            }
        }

        for ($i=0; $i < strlen($out); $i++) {
            $sum += $out[$i];
        }

        $indentificator = ($sum+($sum*9)%10)%10;
        if($indentificator == 0){
            $debit_valid = true;
        }else{
            $debit_valid = false;
        }
        return $debit_valid;
    }

    public function luhn( $zahl )
    {
        $zahl = $zahl*2;
        if ($zahl > 9){
            $zahl1 = substr($zahl, 0, 1);
            $zahl2 = substr($zahl, 1, 2);
            $zahl = $zahl1 + $zahl2;
        }
        return $zahl;
    }

}