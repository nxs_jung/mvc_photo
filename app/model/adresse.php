<?php

class Adresse extends DatabaseObject{

    protected static $table_name = 'adresse';
    protected static $db_fields = array('id','street', 'number', 'plz', 'ort', 'country');

    public $id;
    public $street;
    public $number;
    public $plz;
    public $ort;
    public $country;

    protected function create(){
        $database = Registry::make('MySQLDatabase');
        // Don't forget your SQL syntax and good habits:
        // - INSERT INTO table (key, key) VALUES ('value', 'value')
        // - single-quotes around all values
        // - escape all values to prevent SQL injection
        $attributes = $this->sanitized_attributes();
        array_shift($attributes);

        $sql = "INSERT INTO ".static::$table_name." (";
        $sql .= join(", ", array_keys($attributes));
        $sql .= ") VALUES ('";
        $sql .= join("', '", array_values($attributes));
        $sql .= "')";

        if($database->query($sql)){
            $this->id = $database->insert_id();
            return true;
        }else{
            return false;
        }
    }

}