<?php
class Comment extends DatabaseObject {

    protected static $table_name = "comments";
    protected static $db_fields = array('id', 'photograph_id', 'created', 'author', 'body');

    public $id;
    public $photograph_id;
    public $created;
    public $author;
    public $body;

    /**
     * Erstellt ein Comment-Objekt
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @param  int $photo_id die id des Fotos, welches kommentiert worden ist
     * @param  string $author   Der Autor des Kommentars
     * @param  string $body     Der Kommentar
     * @return Objekt/boolean
     */
    public static function make($photo_id, $author="Anymous" , $body=""){
        if(!empty($photo_id) && !empty($author) && !empty($body)){
            $comment = new Comment();
            $comment->photograph_id = (int) $photo_id;
            $comment->created = strftime("%Y-%m-%d %H:%M:%S", time());
            $comment->author = $author;
            $comment->body = $body;
            return $comment;
        }else{
            return false;
        }
    }

    /**
     * Gibt alle Comments für ein Foto zurück
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @param  integer $photo_id Die id des Fotos
     * @return Ressource
     */
    public static function find_comments_on($photo_id=0){
        $database = Registry::make('MySQLDatabase');

        $sql = "SELECT * FROM ".static::$table_name;
        $sql .= " WHERE photograph_id=".$database->escape_value($photo_id);
        $sql .= " ORDER BY created ASC";
        return static::find_by_sql($sql);
    }



}