<?php

class Cart extends DatabaseObject {

    protected static $table_name = 'cart';
    protected static $db_fields = array('id','user_id', 'type', 'product_id');

    public $id;
    public $user_id;
    public $type;
    public $product_id;

    private $delimiter;
    private $basket_array;

    public function __construct($user_id = 0)
    {
        $session = Registry::make('Session');
        if($user_id == 0){
            $user_id = $session->user_id;
        }

        $this->user_id = $user_id;
        $this->delimiter = "|";


        if(isset($session->cart->basket)){
            $this->basket = $session->cart->basket;
        }else{
            $this->basket = "";
        }
    }

    public function add_product($product){
        $this->type = $product->type;
        $this->product_id = $product->id;
        $this->save();
    }

    private function basket_to_array(){
        $session = Registry::make('Session');

        $aArray = $this->find_baskets_by_user_id($session->user_id);

        $i = 0;
        if($aArray){
            foreach ($aArray as $order) {
                foreach ($order as $key => $value) {
                    if($key === "id" || $key === "user_id" || $key == "type" || $key == "product_id"){
                        $this->basket_array[$i][$key] = $value;
                    }
                }
                $i++;
            }
        }
        //if(count($this->basket) > 1){
        //    $this->basket_array = implode("".$this->delimiter, $this->basket);
        //}
    }

    private function bArray_to_string(){
        if(count($this->basket_array) > 1){
            $this->basket = explode($this->delimiter, $this->basket_array);
        }
    }

    public function display_basket(){
        $this->basket_to_array();
        $output = "<ol>";
        if(count($this->basket_array) >= 1){
            foreach ($this->basket_array as $index => $product) {
                $output .= "<li data-index=\"".$product['id']."\">";

                $obj = ((substr($product['type'], 0, 5) == "image")
                        ? Photograph::find_by_id($product['product_id'])
                        : Video::find_by_id($product['product_id'])
                    );

                $output .= $obj->filename." | ".$obj->type." | ".$obj->caption;
                $output .= " <a href=\"index.php?cl=shop&fnc=delete_from_basket&id=".$product['id']."\" >Delete</a>";
                $output .= "</li>";
            }
        }else{
            $output .= "<h2>Der Einkaufkorb ist leer!</h2>";
        }
        $output .= "</ol>";
        return $output;
    }



    public static function find_baskets_by_user_id($user_id){
        $database = Registry::make('MySQLDatabase');
        $query = "SELECT * FROM ".static::$table_name." WHERE user_id=".$database->escape_value($user_id);
        $resource = $database->query($query);
        while ($row = $database->fetch_array($resource)) {
            $result_array[] = $row;
        }
        return !empty($result_array) ? $result_array: false ;
    }

    public function save(){
        // A new record won't have an id yet
        $this->bArray_to_string();
        return isset($this->id) ? $this->update() : $this->create();
    }

    protected function create(){
        $database = Registry::make('MySQLDatabase');
        // Don't forget your SQL syntax and good habits:
        // - INSERT INTO table (key, key) VALUES ('value', 'value')
        // - single-quotes around all values
        // - escape all values to prevent SQL injection
        $attributes = $this->sanitized_attributes();
        array_shift($attributes);

        $sql = "INSERT INTO ".static::$table_name." (";
        $sql .= join(", ", array_keys($attributes));
        $sql .= ") VALUES ('";
        $sql .= join("', '", array_values($attributes));
        $sql .= "')";

        if($database->query($sql)){
            $this->id = $database->insert_id();
            return true;
        }else{
            return false;
        }
    }
}
