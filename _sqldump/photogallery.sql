-- --------------------------------------------------------
-- Host:                         192.168.5.201
-- Server Version:               5.5.40-0ubuntu0.12.04.1-log - (Ubuntu)
-- Server Betriebssystem:        debian-linux-gnu
-- HeidiSQL Version:             8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Exportiere Datenbank Struktur für prjdb_azubi_moj_00_oop
DROP DATABASE IF EXISTS `prjdb_azubi_moj_00_oop`;
CREATE DATABASE IF NOT EXISTS `prjdb_azubi_moj_00_oop` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `prjdb_azubi_moj_00_oop`;


-- Exportiere Struktur von Tabelle prjdb_azubi_moj_00_oop.comments
DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `photograph_id` int(11) NOT NULL,
  `vid_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `author` varchar(255) NOT NULL,
  `body` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `photograph_id` (`photograph_id`),
  KEY `vid_id` (`vid_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Exportiere Daten aus Tabelle prjdb_azubi_moj_00_oop.comments: ~3 rows (ungefähr)
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
REPLACE INTO `comments` (`id`, `photograph_id`, `vid_id`, `created`, `author`, `body`) VALUES
	(5, 7, 0, '2014-11-18 15:33:51', 'Mo', 'Hey!'),
	(6, 7, 0, '2014-11-18 15:40:03', 'Mary', 'i Like it too!'),
	(7, 1, 0, '2014-11-24 13:47:05', 'Hey', 'asdfklasjdfÃ¶lkjasdf');
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;


-- Exportiere Struktur von Tabelle prjdb_azubi_moj_00_oop.photographs
DROP TABLE IF EXISTS `photographs`;
CREATE TABLE IF NOT EXISTS `photographs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(255) NOT NULL,
  `type` varchar(100) NOT NULL,
  `size` int(11) NOT NULL,
  `caption` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- Exportiere Daten aus Tabelle prjdb_azubi_moj_00_oop.photographs: ~10 rows (ungefähr)
/*!40000 ALTER TABLE `photographs` DISABLE KEYS */;
REPLACE INTO `photographs` (`id`, `filename`, `type`, `size`, `caption`) VALUES
	(7, 'Desert.jpg', 'image/jpeg', 845941, 'Desert'),
	(8, 'Hydrangeas.jpg', 'image/jpeg', 595284, 'Blumen!'),
	(9, 'Tulips.jpg', 'image/jpeg', 620888, 'Noch mehr Blumen'),
	(11, 'Chrysanthemum.jpg', 'image/jpeg', 879394, 'Flowers'),
	(12, 'Jellyfish.jpg', 'image/jpeg', 775702, 'Jelly jelly jellyfish!!'),
	(13, 'Koala.jpg', 'image/jpeg', 780831, 'Ko'),
	(14, 'Penguins.jpg', 'image/jpeg', 777835, 'Es ist ein Pinguin!'),
	(15, 'Lighthouse.jpg', 'image/jpeg', 561276, 'Leuchtturm'),
	(16, '535920_725243460878259_3269758550655821486_n.jpg', 'image/jpeg', 80926, 'Text'),
	(22, 'Chrysanthemum.jpg', 'image/jpeg', 879394, '13f14');
/*!40000 ALTER TABLE `photographs` ENABLE KEYS */;


-- Exportiere Struktur von Tabelle prjdb_azubi_moj_00_oop.role
DROP TABLE IF EXISTS `role`;
CREATE TABLE IF NOT EXISTS `role` (
  `bezeichnung` varchar(50) NOT NULL,
  `photo` int(11) NOT NULL,
  `video` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  PRIMARY KEY (`bezeichnung`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportiere Daten aus Tabelle prjdb_azubi_moj_00_oop.role: ~7 rows (ungefähr)
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
REPLACE INTO `role` (`bezeichnung`, `photo`, `video`, `user`) VALUES
	('pho', 1, 0, 0),
	('pho-user', 1, 0, 1),
	('pho-vid', 1, 1, 0),
	('pho-vid-user', 1, 1, 1),
	('user', 0, 0, 1),
	('vid', 0, 1, 0),
	('vid-user', 0, 1, 1);
/*!40000 ALTER TABLE `role` ENABLE KEYS */;


-- Exportiere Struktur von Tabelle prjdb_azubi_moj_00_oop.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(40) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `mail` varchar(50) NOT NULL,
  `role` varchar(50) NOT NULL,
  `admin` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `mail` (`mail`),
  KEY `role` (`role`),
  CONSTRAINT `role` FOREIGN KEY (`role`) REFERENCES `role` (`bezeichnung`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- Exportiere Daten aus Tabelle prjdb_azubi_moj_00_oop.users: ~3 rows (ungefähr)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
REPLACE INTO `users` (`id`, `username`, `password`, `first_name`, `last_name`, `mail`, `role`, `admin`) VALUES
	(1, 'admin', 'admin1', 'Moritz', 'Jung', 'jung@nexus-netsoft.com', 'pho-vid-user', 2),
	(3, 'johnsmith', '12345', 'John', 'Smith', 'test@test.com', 'vid', 0),
	(11, 'adminsdf', '123432', 'sdfsda', 'df', 'sdf23f@wed.de', 'pho-vid', 0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;


-- Exportiere Struktur von Tabelle prjdb_azubi_moj_00_oop.videos
DROP TABLE IF EXISTS `videos`;
CREATE TABLE IF NOT EXISTS `videos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(255) NOT NULL,
  `type` varchar(100) NOT NULL,
  `size` int(11) NOT NULL,
  `caption` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Exportiere Daten aus Tabelle prjdb_azubi_moj_00_oop.videos: ~1 rows (ungefähr)
/*!40000 ALTER TABLE `videos` DISABLE KEYS */;
REPLACE INTO `videos` (`id`, `filename`, `type`, `size`, `caption`) VALUES
	(1, '00_01_inroduction.mov', 'video/quicktime', 1875210, 'intro');
/*!40000 ALTER TABLE `videos` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
