<?php

require_once 'bootstrap.php';



if(isset($_GET['cl']) && class_exists($_GET['cl']) )
{
    $class = $_GET['cl'];
}
elseif(isset($_GET['cl']))
{
    $class = 'error';
}
else
{
    $class = 'home';
}

$obj = new $class();

$obj->init();

if(isset($_GET['fnc']) && method_exists($obj, $_GET['fnc'])){
    call_user_func(array($obj, $_GET['fnc']));
}

$obj->render();

