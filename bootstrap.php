<?php

error_reporting(E_ALL|E_STRICT);
ini_set('display_errors', 1);

require_once "vendor/autoload.php";
require_once "config.php";

require_once 'app/core/load.php';
require_once 'app/core/controller.php';
require_once 'app/core/backend_controller.php';
require_once 'app/core/database_object.php';
require_once 'app/core/database.php';
require_once 'app/core/functions.php';
require_once 'app/core/logger.php';
require_once 'app/core/validator.php';
require_once 'app/core/validator_user.php';
require_once 'app/core/validator_payment.php';
require_once 'app/core/registry.php';
require_once 'app/controller/frontend/home.php';
require_once 'app/controller/frontend/login.php';
require_once 'app/controller/frontend/photo.php';
require_once 'app/controller/frontend/video.php';
require_once 'app/controller/frontend/shop.php';
require_once 'app/controller/backend/homeback.php';
require_once 'app/controller/backend/photolist.php';
require_once 'app/controller/backend/videolist.php';
require_once 'app/controller/backend/userManagement.php';
require_once 'app/controller/backend/logfile.php';
require_once 'app/model/photograph.php';
require_once 'app/model/role.php';
require_once 'app/model/session.php';
require_once 'app/model/user.php';
require_once 'app/model/videos.php';
require_once 'app/model/comment.php';
require_once 'app/model/cart.php';
require_once 'app/model/adresse.php';
